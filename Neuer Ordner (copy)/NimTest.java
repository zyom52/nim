import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class NimTest {
    int[] r = {2,6,7,4,3,8,6};

    @Test
    void array_biger_than_zero(){
        Nim nim = new Nim(r);
        assertTrue(nim.rows.length >= 1);
    }


    @Test
    void move_test(){

        assertTrue(Move.of(4,8) instanceof Move);
    }



   // @Test
   /* void move_is_work(){
        int row = 3;
        int number =7;

        Nim nim = new Nim(r);
        Move move = new Move(nim.randomRow,nim.randomNumber);

        assertEquals(nim.randomMove(), move);
    }*/


    @Test
    void  is_Game_Over(){
        Nim nim = new Nim(r);
        assertFalse(Arrays.stream(nim.rows).allMatch(n -> n == 0));
    }
    @Test
    void play_method_test(){
        Nim nim = new Nim(5);

        int row = 0;
        int number = 2;
        Move m = new Move(row,number);
        Nim nim1 = nim.play(m);

      assertEquals(nim1.rows[0],3);

    }

    @Test
    void random_test(){
        Nim nim = new Nim(r);
        Move m = nim.randomMove();



       Nim nim1 =  nim.play(m);
        assertEquals(nim1,nim.of(m.getRow()));
    }

}
/*
*  public Move randomMove() {
        assert !isGameOver();
        int row;
        do {
            row = r.nextInt(rows.length);

        } while (rows[row] == 0);
        int number = r.nextInt(rows[row]) + 1;

        return Move.of(row, number);
    }*/
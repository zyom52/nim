import java.util.Random;

interface NimView {
    void registerGame(int[] rows);

    void drawGame(int row, int number);

    void drawGG(boolean won);
}

interface NimController {

    void nextFrame();

    void userInput(int row, int number);
}

public class Controller implements NimController {
    private final NimView view;
    private NimGame game;
    private boolean pcsTurn;

    Controller(NimView view) {
        this.pcsTurn = false;
        this.view = view;
        int[] rows = new int[5];
        Random r = new Random();
        for (int i = 0; i < rows.length; i++)
            rows[i] = r.nextInt(1, 10);
        this.game = Nim.of(rows);
        this.view.registerGame(rows);
    }

    @Override
    public void nextFrame() {
        if (pcsTurn) {
            Move m = game.bestMove();
            game = game.play(m);
            pcsTurn = false;
            view.drawGame(m.row, m.number);
        } else view.drawGame(0, 0);
        if (game.isGameOver()) {
            view.drawGG(pcsTurn);
        }
    }

    @Override
    public void userInput(int row, int number) {
        game = game.play(Move.of(row, number));
        pcsTurn = true;
    }
}

import processing.core.PApplet;
import processing.core.PImage;
import processing.event.KeyEvent;
import processing.event.MouseEvent;

import java.util.Arrays;

public class View extends PApplet implements NimView {

    public static void main(String[] args) {
        PApplet.main(View.class);
    }

    private NimController controller;
    private PImage image;
    private int[][] field;
    private int sizeH, sizeW;
    private int row = -1, number;

    public View() {
        setSize(600, 600);
    }

    @Override
    public void setup() {
        this.controller = new Controller(this);
        noLoop();
    }

    @Override
    public void draw() {
        controller.nextFrame();
    }

    @Override
    public void mouseClicked(MouseEvent event) {

        int x = event.getX() / sizeW;
        int y = event.getY() / sizeH;


        if (field[y][x] == 1 && (row == -1 || row == y)) {
            row = y;
            field[y][x] = -1;
            number++;
        } else if (field[y][x] == -1) {
            field[y][x] = 1;
            if (--number <= 0) row = -1;
        }
        redraw();
    }

    @Override
    public void keyPressed(KeyEvent event) {
        if ((keyCode == ENTER || keyCode == ' ') && number > 0) {
            for (int i = 0; i < field[row].length; i++)
                if (field[row][i] == -1) field[row][i] = 0;
            controller.userInput(row, number);
            row = -1;
            number = 0;
            redraw();
        }
    }

    @Override
    public void registerGame(int[] rows) {
        int max = Arrays.stream(rows).max().getAsInt();
        image = loadImage("pngwing.png");
        image.resize(width / max, height / rows.length);

        sizeH = height / rows.length;
        sizeW = width / max;

        field = new int[rows.length][max];

        for (int i = 0; i < rows.length; i++) {
            for (int j = 0; j < rows[i]; j++) {
                field[i][j] = 1;
            }
        }
    }

    @Override
    public void drawGame(int row, int number) {
        background(100);

        // pc's turn
        for (int i = 0; i < field[row].length; i++) {
            if (number == 0) break;
            if (field[row][i] == 1) {
                field[row][i] = 0;
                number--;
            }
        }

        // draw field
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                switch (field[i][j]) {
                    case 1 -> image(image, j * sizeW, i * sizeH);
                    case -1 -> {
                        fill(0);
                        rect(j * sizeW + sizeW / 3.0F, i * sizeH + sizeH / 6.0F, sizeW / 3.0F, sizeH / 1.5F, 15.0F);
                    }
                    default -> {
                    }
                }
            }
        }
    }

    @Override
    public void drawGG(boolean won) {
        background(0);
        textSize(64);
        fill(255);
        text("GG YOU " + (won ? "WON" : "LOST") + "!", width / 4.0F, height / 2.0F);
    }
}
